import site
import sys
import os

mach_home = '/operation/machine'
macharch = 'debian8'
os.environ['MACH_HOME'] = mach_home
os.environ['MACHARCH'] = macharch
os.environ['APPHOME'] = os.path.join('/operation', 'appdata')
os.environ['PATH'] = os.pathsep.join(
    (os.environ['PATH'], os.path.join(mach_home, 'scripts'), os.path.join(mach_home, 'bin', macharch)))
os.environ['http_proxy'] = "http://proxy.esrf.fr:3128/"
os.environ['https_proxy'] = "http://proxy.esrf.fr:3128/"
os.environ['no_proxy'] = "esrf.fr,local,localhost"

pyvers = os.path.join('python{0}.{1}'.format(*sys.version_info[0:2]), 'site-packages')
mach_prefix = os.path.join(mach_home, 'lib', pyvers)
mach_exec_prefix = os.path.join(mach_home, 'lib', macharch, pyvers)
sys.path.insert(1, mach_prefix)
sys.path.insert(1, mach_exec_prefix)
site.addsitedir(mach_prefix)
site.addsitedir(mach_exec_prefix)
