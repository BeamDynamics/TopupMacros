"""Topup control

This module is imported at macroserver startup and on "ReloadMacroLib"

Devices involved in topup control:
    sys/macroserver/topup
    sys/door-topup/01
    sys/door-topup/02
    sys/countdowncron/01
    sys/machstat/tango	User countdown
    sys/countdown/simu	Simulation countdown

Environment variables:
   current_ref      total current [mA]
   sb_current_ref   single bunch current [mA]
   period           top-up periodicity [mn]
   clean            Cleaning in the SR (Boolean)
   mode             'hybrid' | '7/8+1' | 'other'
   SYSparkThreshold Booster current threshold for cleaning
   EnableLinac
   EnableSYInj
   EnableBpss
   EnableRips
   EnableSyrf
   EnableScrapers        Act on scrapers
   EnableCheckSYCleaning Check Cleaning in the booster
   EnableSYCleaning      Cleaning in the booster
   CheckDelay            Delay after injection for checking the standby state of SYRF and BPSS [s]
   min_current           minimum current for using the measured filling patterm
   max_age               maximum age of the measured filling pattern [s]
   min_booster_current   minimum curren in the booster to allow injection
"""

from __future__ import print_function

import os
import math
import threading
import time
import datetime
from functools import partial
import numpy as np

from sardana.macroserver.macro import Type

from pymach.tango import Device, DeviceGroup, Attribute, DevState, DevFailed, DevSource, AttrQuality
from pymach.devutil import is_notmoving
from pymach.timing import wait_for, TimeoutError
from srfill import SRFill
from srinj import SRInj
from syext import SYExt
from syinj import SYInj

from OPEMacro import OPEMacro, OPEError

import PyTaco  # for pulse_select

_SCRAPER_LIST = ('sr/d-scr/c4-int', 'sr/d-scr/c4-ext', 'sr/d-scr/c5-up',
                'sr/d-scr/c5-low', 'sr/d-scr/c22-up', 'sr/d-scr/c25-up',
                'sr/d-scr/c25-low')


class TopupMacro(OPEMacro):
    """Base class for \"topup\" macros
    Connects to several devices,
    Records startup time
    """
    clname = 'topup'
    sy_clean = False
    next_injection_time = datetime.datetime.fromtimestamp(0)
    syinj = SYInj()
    syext = SYExt()
    srinj = SRInj()
    srfill = SRFill()
    bpss = Device('sy/bpss/manager', source=DevSource.DEV)
    rips = Device('sy/ps-rips/manager')
    syrf = Device('sy/rfssa-tra/tra0', source=DevSource.DEV)
    linac = Device('elin/master/op', source=DevSource.DEV)
    gun = Device('elin/beam/run', source=DevSource.DEV)
    cron = (Device('sys/countdowncron/01'), Device('sys/countdowncron/02'))
    machstat = Device('sys/machstat/tango')
    idservers = DeviceGroup('IDs', ['id/id/{0}'.format(n) for n in machstat.get_property('ActiveID')['ActiveID']])
    scrapers = DeviceGroup('Scrapers',_SCRAPER_LIST)
    idpower = Device('id/powert/all')
    SRclean = Device('sr/d-mfdbk/cleaning', source=DevSource.DEV)
    SYclean = Device('sy/d-clean/master', source=DevSource.DEV)
    SYcleaning_settings = Device('sys/settings/sy-cleaning')
    SYcheck = Device('sy/d-clean/check')
    devpulses = [PyTaco.Device(dev) for dev in
                          ['sy/t-inj/pulse_select', 'sy/t-inj/pulse_1', 'sy/t-inj/pulse_2', 'sy/t-inj/pulse_3',
                           'sy/t-inj/pulse_4', 'sy/t-inj/pulse_5']]
    [dev.open(8) for dev in devpulses] 
    conv = np.array([1, 3, 7, 15, 31], dtype=np.int16)

    # scr4 = Device('sr/d-scr/c4-ext')  # for 7/8 experiment
    # c4ext_position = 36  # for 7/8 experiment

    @staticmethod
    def wait2nextinj(period):
        """Synchronise the next injection time with sharp hours
        """
        now = time.time()
        nowvec = time.localtime(now)
        nowsec = 3600 * (15 + nowvec.tm_hour) + 60 * nowvec.tm_min + nowvec.tm_sec
        injsec = math.ceil(float(nowsec) / period) * period
        waitsec = injsec - nowsec
        if waitsec < 220:
            waitsec = waitsec + period
        TopupMacro.next_injection_time = datetime.datetime.fromtimestamp(now + waitsec)
        return waitsec

    def __init__(self, *args, **kwargs):
        super(TopupMacro, self).__init__(*args, **kwargs)
        self.start_time = time.time()

    def set_counter(self, period=None, cronid=0):
        """Set the \"countdown\" for the next injection """
        if period is None:
            period = self.getEnv('period')
        if period > 0:
            counter_name = self.cron[cronid].get_property('CountDownName')['CountDownName'][0]
            counter = Device(counter_name)
            wt = self.wait2nextinj(60 * period)
            try:
                counter.CountDown = wt
            except DevFailed as deverr:
                self.deverror(deverr, 'Error in resetting user countdown')
            else:
                msg = 'Next injection at {0:%H:%M}'.format(self.next_injection_time)
                self.output(msg)

    # noinspection PyPep8Naming
    def BPSS_ready(self, run=False):
        """Check BPSS state
        """
        state = self.bpss.state()
        ready = False
        if state in (DevState.MOVING, DevState.RUNNING):
            pass
        elif state == DevState.ON:
            if (self.bpss.SettingsFileName != 'Operation') and run:
                self.output("Set BPSS in 'Operation'")
                self.bpss.Operation()
            else:
                self.info("BPSS is On in {0:.1f} seconds".format(time.time() - self.start_time))
                ready = True
        elif (state == DevState.STANDBY) and not run:
            self.output("Set BPSS On")
            self.bpss.On()
        elif (state == DevState.OFF) and not run:
            self.output("Set BPSS in Standby")
            self.bpss.Standby()
        else:
            raise OPEError("BPSS is not ready: state {0}".format(state))
        return ready

    # noinspection PyPep8Naming
    def RIPS_ready(self, run=False):
        """Check RIPS state
        """
        state = self.rips.state()
        ready = False
        if state in (DevState.MOVING, DevState.INIT):
            pass
        # elif state == DevState.ON and run:
        #     self.rips.StartRamping()
        elif state in (DevState.RUNNING, DevState.ON):
            ready = True
        elif state == DevState.STANDBY:
            self.output("Set RIPS On")
            self.rips.On()
        elif (state == DevState.OFF) and not run:
            self.output("Set RIPS in Standby")
            self.rips.Standby()
        else:
            raise OPEError("RIPS is not ready: state {0}".format(state))
        return ready

    # noinspection PyPep8Naming
    def SYRF_ready(self, run=False):
        """Check SYRF state
        """
        state = self.syrf.state()
        ready = False
        if state in (DevState.MOVING, DevState.RUNNING):
            pass
        elif state == DevState.ON:
            self.info("SYRF is On in {0:.1f} seconds".format(time.time() - self.start_time))
            ready = True
        elif state == DevState.STANDBY:
            if run:
                self.output("Set SYRF On")
                self.syrf.On()
            else:
                self.info("SYRF is in Standby in {0:.1f} seconds".format(time.time() - self.start_time))
                ready = True
        elif (state == DevState.OFF) and not run:
            self.output("Set SYRF in Standby")
            self.syrf.Standby()
        else:
            raise OPEError("SYRF is not ready: state {0}".format(state))
        return ready

    def linac_ready(self, run=False):
        """Check Linac state
        """
        state = self.linac.confirmedState()
        ready = False
        if state == DevState.MOVING:
            pass
        elif state == DevState.RUNNING:
            self.info("Linac is On in {0:.1f} seconds".format(time.time() - self.start_time))
            ready = True
        elif state == DevState.STANDBY:
            if run:
                self.output("Set Linac On")
                self.linac.WarmUp()  # Set in RUNNING
            else:
                self.info("Linac is in Standby in {0:.1f} seconds".format(time.time() - self.start_time))
                ready = True
        elif (state == DevState.INIT) and not run:  # in LowHeating state
            self.output("Set Linac in Standby")
            self.linac.Standby()
        elif state == DevState.ON:
            self.linac.WarmUp()
        else:
            raise OPEError("Linac is not ready: state {0}".format(state))
        return ready

    def check_sycleaning(self,npulse=''):
        threshold = self.getEnv('SYSparkThreshold', 2.e-7)
        main_delay = self.SYclean.MainDelay
        self.SYclean.MainDelay = main_delay - 2
        self.SYcheck.ClearBuffer()
        time.sleep(6.0)
        datam2 = self.SYcheck.GetNLastElem(20)
        # noinspection PyTypeChecker
        m2 = all(datam2 < threshold)
        self.SYclean.MainDelay = main_delay + 2
        self.SYcheck.ClearBuffer()
        time.sleep(6.0)
        datap2 = self.SYcheck.GetNLastElem(20)
        # noinspection PyTypeChecker
        p2 = all(datap2 < threshold)
        self.SYclean.MainDelay = main_delay
        # nux = Attribute('sy/bmcm/1/Nux').value
        f = self.filepath(os.path.join('SYCleaning', '{0:%Y%m%d_%H%M}'.format(self.next_injection_time)))
        np.savetxt('_'.join((f, 'm2'+npulse+'.txt')), datam2)
        np.savetxt('_'.join((f, 'p2'+npulse+'.txt')), datap2)
        # np.savetxt('_'.join((f, 'nux')), nux)
        return m2, p2

    def setclock(self, pulse_list):
        nb_pulses = len(pulse_list)
        data = zip(self.devpulses, np.insert(pulse_list, 0, self.conv[nb_pulses - 1]))
        [devc.cmd('DevSetValue', v) for devc, v in data]
        time.sleep(1.0)

    def setconfig(self,configname):
        try:
            self.SYcleaning_settings.ApplySettings(configname)
        except DevFailed as tangoerror:
            print(tangoerror[0].desc, file=sys.stderr)
        except Exception as err:
            print(err, file=sys.stderr)

    def check_sycleaning_78(self):
        self.gun.Off()
        number_pulse_booster = 2 # 2 booster pulses
        booster_spacing = 206
        pulse_list = np.arange(number_pulse_booster, dtype=np.int16) * booster_spacing
        self.setclock(pulse_list)
        self.setconfig('2bunches4hybrid7_8th')
        self.gun.On()
        m22,p22 = self.check_sycleaning('2')
        ok2 = m22 or p22
        self.gun.Off()
        number_pulse_booster = 4 # 4 booster pulses
        booster_spacing = 88
        pulse_list = np.arange(number_pulse_booster, dtype=np.int16) * booster_spacing
        self.setclock(pulse_list)
        self.setconfig('4bunches4hybrid7_8th')
        self.gun.On()
        m24,p24 = self.check_sycleaning('4')
        ok4 = m24 or p24
        self.gun.Off()
        number_pulse_booster = 1 # 1 booster pulses
        pulse_list = np.arange(number_pulse_booster, dtype=np.int16)
        self.setconfig('single4hybrid7_8th')
        self.setclock(pulse_list)
        self.gun.On()
        m21,p21 = self.check_sycleaning('1')
        ok1 = m21 or p21
        self.gun.Off()
        return ok1,ok2,ok4

 


class BPSSOn(TopupMacro):
    """Set BPSS in 'Operation' - 64s"""

    param_def = [
        ["Run", Type.Boolean, True, "Run"],
        ["Timeout", Type.Float, 100.0, "Timeout for startup"],
    ]

    def run(self, run, timeout):
        if self.getEnv('EnableBpss', True):
            self.try_(wait_for, partial(self.BPSS_ready, run=run), timeout=timeout,
                      message="setting BPSS in 'Operation'")
        if self.getEnv('EnableRips', True):
            self.try_(wait_for, partial(self.RIPS_ready, run=run), timeout=timeout,
                      message="setting RIPS in 'Operation'")


class SYPSOff(TopupMacro):
    """Set BPSS in 'Economy'"""

    def run(self):
        if self.getEnv('EnableBpss', True):
            self.output("Set BPSS in 'Economy'")
            self.try_(self.bpss.Economy)
            # if self.getEnv('EnableRips', True):
            #     self.output("Set RIPS in Standby")
            #     self.try_(self.rips.Standby)


class SYRFOn(TopupMacro):
    """Set SYRF On if necessary - 39 s from STANDBY, 110 s from OFF"""

    param_def = [
        ["Run", Type.Boolean, True, "Run"],
        ["Timeout", Type.Float, 50.0, "Timeout for startup"],
    ]

    def run(self, run, timeout):
        if self.getEnv('EnableSyrf', True):
            self.try_(wait_for, partial(self.SYRF_ready, run=run), timeout=timeout)


class SYRFOff(TopupMacro):
    """Set SYRF in Standby"""

    def run(self):
        if self.getEnv('EnableSyrf', True):
            self.output("Set SYRF in Standby")
            self.try_(self.syrf.Standby)


class LinacOn(TopupMacro):
    """Start Linac - 20 s"""

    param_def = [
        ["Run", Type.Boolean, True, "Run"],
        ["Timeout", Type.Float, 25.0, "Timeout for startup"],
    ]

    def run(self, run, timeout):
        if self.getEnv('EnableLinac', True):
            self.try_(wait_for, partial(self.linac_ready, run=run), timeout=timeout)


class LinacOff(TopupMacro):
    """Stop Linac"""

    def run(self):
        if self.getEnv('EnableLinac', True):
            self.output('Set Linac in Standby')
            self.try_(self.linac.Standby)


class IDCommand(TopupMacro):
    mode = dict(Delivery=1, Topup=2, Injection=3, TestDelivery=1, TestInjection=3)

    param_def = [["command", Type.String, "Delivery", "command name"]]

    def run(self, cmdname):
        def ids_ready():
            return all(st == self.mode[cmdname] for st, y in zip(self.idservers.attr_value('Mode'), ok) if y)

        try:
            self.info('Set IDs to {0}'.format(cmdname))
            result = self.idservers.command_inout(cmdname)
            ok = [not a.has_failed() for a in result]
            if not all(ok):
                self.error('{1} failed on {0}'.format([a.dev_name() for a in result if a.has_failed()], cmdname))
            wait_for(ids_ready, timeout=60)
            self.info('Gaps in {1} in {0:.1f} seconds'.format(time.time() - self.start_time, cmdname))
        except TimeoutError:
            self.error("Timeout setting IDs to {0}".format(cmdname))

#class GroupCommand(TopupMacro):
#    mode = dict(Delivery=1, Topup=2, Injection=3, TestDelivery=1, TestInjection=3)
#
#    param_def = [["device", Type.String, "IDs", "device name"]]
#    param_def = [["command", Type.String, "Delivery", "command name"]]
#
#    def run(self,devname,cmdname):
#
#        servers = []
#        if devnames=='IDs':
#          servers = self.idservers
#        elif devnames=='Scrapers':
#          servers = self.scrapers
#
#        def devices_ready():
#            return all(st == self.mode[cmdname] for st, y in zip(servers.attr_value('Mode'), ok) if y)
#    
#        try:
#            self.info('Set devices to {0}'.format(cmdname))
#            result = servers.command_inout(cmdname)
#            ok = [not a.has_failed() for a in result]
#            if not all(ok):
#                self.error('{1} failed on {0}'.format([a.dev_name() for a in result if a.has_failed()], cmdname))
#            wait_for(devices_ready, timeout=60)
#            self.info('Devices in {1} in {0:.1f} seconds'.format(time.time() - self.start_time, cmdname))
#        except TimeoutError:
#            self.error("Timeout setting devices to {0}".format(cmdname))


class GetTopupEnv(TopupMacro):
    def run(self):
        self.info(str(self.getAllEnv()))


class TestInjTime(TopupMacro):
    param_def = [["Period", Type.Integer, 20, "Injection periodicity [mn]"]]

    def run(self, period):
        temp = self.next_injection_time  # save current injection time
        # noinspection PyUnusedLocal
        wt = self.wait2nextinj(60 * period)
        message = 'Next injection at {0:%Y%m%d_%H%M}'.format(self.next_injection_time)
        TopupMacro.next_injection_time = temp  # restore the current injection time
        self.output(message)


class SetPeriod(TopupMacro):
    """Set topup periodicity"""

    param_def = [["Period", Type.Integer, 20, "Injection periodicity [mn]"]]

    def run(self, period):
        self.setEnv('period', period)


class SetCurrent(TopupMacro):
    """Set the desired SR current"""

    param_def = [["Current", Type.Float, 200.0, "Desired SR current [mA]"]]

    def run(self, current_ref):
        self.setEnv('current_ref', current_ref)


class SetSbCurrent(TopupMacro):
    """Set the desired single bunch current for 7/8+1 mode"""

    param_def = [["SbCurrent", Type.Float, 4.0, "Desired single bunch current [mA]"]]

    def run(self, sb_current_ref):
        self.setEnv('sb_current_ref', sb_current_ref)


class SetCleaning(TopupMacro):
    """Set the cleaning flag"""

    param_def = [["Cleaning", Type.Boolean, False, "Cleaning flag"]]

    def run(self, clean):
        self.setEnv('clean', clean)


class SetThreshold(TopupMacro):
    """Set the booster current threshold"""

    param_def = [["Threshold", Type.Float, 0.03, "Booster threshold curent"]]

    def run(self, threshold):
        self.setEnv('threshold', threshold)


class SetMode(TopupMacro):
    """Set the filling mode"""

    param_def = [["FillingMode", Type.String, "other", "Filling mode"]]

    def run(self, mode):
        allowed = ('other', '7/8+1', 'hybrid', 'top7/8', 'fill7/8')
        for m in allowed:
            if m == mode:
                self.setEnv('mode', repr(mode))
                return
        self.error('FillingMode cannot be "{0}"'.format(mode))


class SRInjOn(TopupMacro):
    """Start SR injection - 8 s"""

    param_def = [
        ["start_kickers", Type.Boolean, False, "Start the kickers immediately"],
    ]

    def run(self, sk):
        move_scrapers = self.getEnv('EnableScrapers', True)
        self.output('Switching S R injection ON')
        # TopupMacro.c4ext_position = self.scr4.Position  # for 7/8 experiment
        try:
            with self.context('Info'):
                self.srinj.on(move_scrapers=move_scrapers, start_kickers=sk)
                # self.srinj.StartKickers = False
                # self.srinj.MoveScrapers = move_scrapers
                # self.srinj.verbose_command('On', stdout=self.notifier['Info'], stderr=self.notifier['Error'])
        except DevFailed as deverr:
            self.deverror(deverr, "Cannot start S R injection:")
        except Exception as err:
            self.error("Cannot start S R injection: {0}".format(err))
        else:
            check, status = self.srinj.getstatus()
            # check = self.srinj.State()
            if check == DevState.STANDBY:
                self.info('SR injection is ON in {0:.1f} seconds'.format(time.time() - self.start_time))
            else:
                self.error("Start of S R injection failed, state {0}".format(check))
                f = self.filepath(os.path.join('Macro_logs', '{0:%Y%m%d_%H%M}'.format(self.next_injection_time)))
                np.savetxt('_'.join((f, 'srinjon.txt')), [status],delimiter=' ',fmt='%s')


class SRInjOff(TopupMacro):
    """Stop SR injection - 4 s"""

    param_def = [
        ["move_scrapers", Type.Boolean, True, "Open scrapers for delivery"],
        ["off_magnets", Type.Boolean, True, "Set kickers Off"],
        ["cleaning_scrapers", Type.Integer, -1, "Enumeration for scrapers used for cleaning"],
    ]

    def run(self, ms, off_magnets, cleaning_scrapers):
        self.output('Switching S R injection OFF')
        try:
            with self.context('Info'):
                self.srinj.off(move_scrapers=ms, off_magnets=off_magnets,cleaning_scrapers=cleaning_scrapers)
                # self.srinj.MoveScraper = ms
                # self.srinj.verbose_command('Off', np.array([True, off_magnets], dtype=int),
                #                            stdout=self.notifier['Info'], stderr=self.notifier['Error'])
        except DevFailed as deverr:
            self.deverror(deverr, "Cannot stop S R injection:")
        except Exception as err:
            self.error("Cannot stop S R injection: {0}".format(err))
        else:
            check, status = self.srinj.getstatus()
            # check = self.srinj.State()
            if check == DevState.OFF:
                self.info('S R injection is OFF in {0:.1f} seconds'.format(time.time() - self.start_time))
            else:
                self.error("Stop of S R injection failed, state {0}".format(check))
                f = self.filepath(os.path.join('Macro_logs', '{0:%Y%m%d_%H%M}'.format(self.next_injection_time)))
                np.savetxt('_'.join((f, 'srinjoff.txt')), [status],delimiter=' ',fmt='%s')


class SYInjOn(TopupMacro):
    """Start Booster injection - 4.3 s"""

    def run(self):
        if self.getEnv('EnableSYInj', True):
            self.output('Switching S Y injection ON')
            try:
                with self.context('Info'):
                    self.syinj.on()
            except DevFailed as deverr:
                self.deverror(deverr, "Cannot start S Y injection:")
            except Exception as err:
                self.error("Cannot start S Y injection: {0}".format(err))
            else:
                check, status = self.syinj.getstatus()
                if check == DevState.ON:
                    self.info('S Y injection is ON in {0:.1f} seconds'.format(time.time() - self.start_time))
                else:
                    self.error("Start of S Y injection failed, state {0}".format(check))
                    f = self.filepath(os.path.join('Macro_logs', '{0:%Y%m%d_%H%M}'.format(self.next_injection_time)))
                    np.savetxt('_'.join((f, 'syinjon.txt')), [status],delimiter=' ',fmt='%s')


class SYInjOff(TopupMacro):
    """Stop Booster injection"""
    param_def = [
        ['off_magnets', Type.Boolean, False, 'Set kickers Off'],
    ]

    def run(self, off_magnets):
        if self.getEnv('EnableSYInj', True):
            self.output('Switching SY injection OFF')
            try:
                with self.context('Info'):
                    self.syinj.off(off_magnets=off_magnets)
            except DevFailed as deverr:
                self.deverror(deverr, "Cannot stop SY injection:")
            except Exception as err:
                self.error("Cannot stop SY injection: {0}".format(err))
            else:
                check, status = self.syinj.getstatus()
                if check == (DevState.OFF if off_magnets else DevState.STANDBY):
                    self.info('SY injection is OFF in {0:.1f} seconds'.format(time.time() - self.start_time))
                else:
                    self.error("Stop of Booster injection failed: state {0}".format(check))
                    f = self.filepath(os.path.join('Macro_logs', '{0:%Y%m%d_%H%M}'.format(self.next_injection_time)))
                    np.savetxt('_'.join((f, 'syinjoff.txt')), [status],delimiter=' ',fmt='%s')


class SYExtOn(TopupMacro):
    """Start Booster extraction - 9.4 s (5.0 s without reset interlocks)"""
    def run(self):
        self.output('Starting SY Extraction')
        if self.getEnv('EnableRips', True):
            self.try_(self.rips.StartRamping, message="starting RIPS ramp")
        try:
            with self.context('Info'):
                self.syext.on()
                # Device('sy/ps-ke/1').Standby()
        except DevFailed as deverr:
            self.deverror(deverr, "Cannot start SY extraction:")
        except Exception as err:
            self.error("Cannot start SY extraction: {0}".format(err))
        else:
            check, status = self.syext.getstatus()
            if check == DevState.RUNNING:
                self.info('SY extraction is ON in {0:.1f} seconds'.format(time.time() - self.start_time))
            else:
                self.error("Start of Booster extraction failed: state {0}".format(check))
                f = self.filepath(os.path.join('Macro_logs', '{0:%Y%m%d_%H%M}'.format(self.next_injection_time)))
                np.savetxt('_'.join((f, 'syexton.txt')), [status],delimiter=' ',fmt='%s')


class SYExtOff(TopupMacro):
    """Stop Booster extraction"""
    param_def = [
        ['off_magnets', Type.Boolean, False, 'Set kickers Off'],
    ]

    def run(self, off_magnets):
        self.output('Switching SY extraction OFF')
        try:
            with self.context('Info'):
                self.syext.off(off_magnets=off_magnets)
        except DevFailed as deverr:
            self.deverror(deverr, "Cannot stop SY extraction:")
        except Exception as err:
            self.error("Cannot stop SY extraction: {0}".format(err))
        else:
            check, status = self.syext.getstatus()
            if check == (DevState.OFF if off_magnets else DevState.STANDBY):
                self.info('SY extraction is OFF in {0:.1f} seconds'.format(time.time() - self.start_time))
            else:
                self.error("Stop of Booster extraction failed: state {0}".format(check))
                f = self.filepath(os.path.join('Macro_logs', '{0:%Y%m%d_%H%M}'.format(self.next_injection_time)))
                np.savetxt('_'.join((f, 'syextoff.txt')), [status],delimiter=' ',fmt='%s')


class Clean(TopupMacro):
    """Clean the beam"""

    def run(self):
        state = self.SRclean.state()
        try:
            if state == DevState.OFF:
                self.output('Start cleaning')
                self.SRclean.DoAll()
                wait_for(lambda: self.SRclean.state() == DevState.OFF, timeout=45)
            else:
                self.error('Cleaning device not ready: {0}'.format(state))
        except TimeoutError:
            self.error('Timeout on cleaning')
        except DevFailed as deverr:
            self.deverror(deverr, "Cleaning failed:")
        else:
            self.info('Cleaning done in {0:.1f} seconds'.format(time.time() - self.start_time))


class CheckBooster(TopupMacro):
    """Test Booster current

    1 s for gun ON
    7 s for start cleaning
    12 s for check cleaning"""

    def run(self):
        def start_booster_cleaning(**kwargs):
            self.SYclean.On()
            wait_for(partial(is_notmoving, self.SYclean), **kwargs)

        def check_booster_cleaning():
            self.SYcheck.Start()
            m2, p2 = self.check_sycleaning()
            ok = m2 or p2
            if not ok:
                self.error('Cleaning in the booster failed')
                msg = 'Refill with bunch cleaning in the Storage Ring'
                self.try_(self.machstat.write_attribute, 'Private_operator_mesg', msg, message='setting user msg')
            elif not m2:
                self.error('Warning: cleaning test minus 2 failed')
            elif not p2:
                self.error('Warning: cleaning test plus 2 failed')
            return ok

        def check_booster_cleaning_78():
            self.SYcheck.Start()
            ok1, ok2, ok4 = self.check_sycleaning_78()
            if not ok1:
                self.error('Cleaning of single pulse in the booster failed')
                msg = 'Refill with bunch cleaning in the Storage Ring'
                self.try_(self.machstat.write_attribute, 'Private_operator_mesg', msg, message='setting user msg')
            elif not ok2:
                self.error('Cleaning of 2 pulses in the booster failed')
                msg = 'Refill with bunch cleaning in the Storage Ring'
                self.try_(self.machstat.write_attribute, 'Private_operator_mesg', msg, message='setting user msg')
            elif not ok4:
                self.error('Cleaning of 4 pulses in the booster failed')
                msg = 'Refill with bunch cleaning in the Storage Ring'
                self.try_(self.machstat.write_attribute, 'Private_operator_mesg', msg, message='setting user msg')
            return ok1 and ok2 and ok4

        if self.getEnv('EnableSYCleaning', False):
            self.try_(start_booster_cleaning, message='starting booster cleaning ON', timeout=12)
            rips_state = self.rips.state()
            if self.getEnv('EnableRips', True) and rips_state!=DevState.RUNNING:
                self.try_(self.rips.StartRamping, message="starting RIPS ramp")
            mode = self.getEnv('mode', 'Other')
            fill_single = self.getEnv('fill_single', False)

            #if self.getEnv('EnableCheckSYCleaning',False):
            #    self.try_(self.gun.On, message='switching gun ON')  # 1 s for gun
            #    self.info('Checking booster cleaning')
            #    if mode =='7/8+1':
            #        TopupMacro.sy_clean = self.try_(check_booster_cleaning_78, message='checking booster cleaning', default=True)
            #    else:
            #        TopupMacro.sy_clean = self.try_(check_booster_cleaning, message='checking booster cleaning', default=True)
            #else

            if fill_single:
                pulse_list = np.arange(1, dtype=np.int16)
                self.setclock(pulse_list)
                self.setconfig('single4hybrid7_8th')
            elif mode == '7/8+1':
                number_pulse_booster = 2 # 2 booster pulses
                booster_spacing = 206
                pulse_list = np.arange(number_pulse_booster, dtype=np.int16) * booster_spacing
                self.setclock(pulse_list)
                self.setconfig('2bunches4hybrid7_8th')
            self.try_(self.gun.On, message='switching gun ON')  # 1 s for gun
            self.info('Checking booster')
            TopupMacro.sy_clean = True
        else:
            rips_state = self.rips.state()
            if self.getEnv('EnableRips', True) and rips_state!=DevState.RUNNING:
                self.try_(self.rips.StartRamping, message="starting RIPS ramp")
            self.try_(self.gun.On, message='switching gun ON')  # 1 s for gun
            TopupMacro.sy_clean = True
        self.info('CheckBooster done in {0:.1f} seconds'.format(time.time() - self.start_time))


class Refill(TopupMacro):
    """Inject"""

    param_def = [
        ["Current", Type.Float, 200, "Desired SR current"],
        ["SbCurrent", Type.Float, 0, "Desired single bunch current"],
        ["Margin", Type.Float, 3.0, "Timeout safety margin [default 3]"],
        ["SYCurrent", Type.Float, 0.0, "Measured Booster current"],
        ["Timeout", Type.Float, 0.0, "Injection Max. duration"],
    ]

    def run(self, current, sb_current, margin, sycurrent, timeout):
        mode = self.getEnv('mode', 'Other')
        try:
            with self.context():
                d = {}
                if self.getEnv('fill_single',False):
                    try:
                        self.info('Filling with single pulse in the linac')
                        self.srfill('topup', current, sb_current=sb_current, shuffle=True,
                                margin=margin, sycurrent=sycurrent, timeout=timeout, **d)
                    except KeyError:
                        self.error('Unknown filling mode: switch to standard filling')
                        self.srfill(mode, current, sb_current=sb_current, shuffle=True,
                                    margin=margin, timeout=timeout, **d)
                        self.setEnv('fill_single', False)
                    except ValueError:
                        self.error('Bunch current measurement failed: switch to standard filling')
                        self.srfill(mode, current, sb_current=sb_current, shuffle=True,
                                    margin=margin, timeout=timeout, **d)
                        self.setEnv('fill_single', False)
                else:
                    self.srfill(mode, current, sb_current=sb_current, shuffle=True,
                            margin=margin, timeout=timeout,**d)
        except TimeoutError:
            self.error('Injection failed: Timed out')
        except DevFailed as deverr:
            self.deverror(deverr, "Injection failed: ")
        except Exception as err:
            self.error("Injection failed failed: {0}".format(err))


class Injection(TopupMacro):
    """Start injection"""
    srct = Attribute('sr/d-ct/1/Current')
    srict = Attribute('sr/d-ct/ict1/Current')
    syct = Attribute('sy/d-ct/1/EndCurrent', default=np.nan)
    #kickelems = DeviceGroup('injelems', 'sr/ps-k1234/k1234', 'sr/ps-si/12', 'sr/ps-si/3', 'sy/ps-ke/1')
    kickelems = DeviceGroup('injelems', 'sr/ps-k1234/k1234', 'sr/ps-si/12', 'sr/ps-si/3')

    param_def = [
        ["Margin", Type.Float, 3.0, "Timeout safety margin [default 3]"],
        ["Timeout", Type.Float, 0, "Injection Max. duration"],
    ]

    mode = dict(Delivery=1, Topup=2, Injection=3, TestDelivery=1, TestInjection=3)
    servers = []
    cmdname='Delivery'
    ok=False

    def devices_ready(self):    
        return all(st == self.mode[self.cmdname] for st, y in zip(self.servers.attr_value('Mode'), self.ok) if y)


    def GroupCommand(self,servers,cmdname):

        self.cmdname = cmdname
        self.servers = servers
    
        try:
            self.info('Set devices to {0}'.format(self.cmdname))
            result = self.servers.command_inout(self.cmdname)
            self.ok = [not a.has_failed() for a in result]
            if not all(self.ok):
                self.error('{1} failed on {0}'.format([a.dev_name() for a in result if a.has_failed()], self.cmdname))
            wait_for(self.devices_ready, timeout=60)
            self.info('Devices in {1} in {0:.1f} seconds'.format(time.time() - self.start_time, self.cmdname))
        except TimeoutError:
            self.error("Timeout setting devices to {0}".format(self.cmdname))


    def run(self, margin, timeout):
        rips_state = self.rips.state()
        if self.getEnv('EnableRips', True) and rips_state!=DevState.RUNNING:
            self.try_(self.rips.StartRamping, message="starting RIPS ramp")
        move_scrapers = self.getEnv('EnableScrapers', True)
        min_booster_current = self.getEnv('min_booster_current', 0.08)
        cleaning = self.getEnv('clean', False) or not TopupMacro.sy_clean
        # cleaning = self.getEnv('clean', False)
        current_ref = self.getEnv('current_ref')
        sb_current_ref = self.getEnv('sb_current_ref')
        move_gaps = False
        counter = 'Failure'
        stpargs = dict(move_scrapers=move_scrapers, keep_running=True)
        stp = threading.Thread(target=self.stop_booster, kwargs=stpargs)
        try:
            # sycurrent = self.syct.value
            curvalues = [v.value if v.quality == AttrQuality.ATTR_VALID else np.nan for v in
                         self.syct.history(50)]
            sycurrent = np.nanmean(curvalues)
            if np.isnan(sycurrent):
                raise OPEError('Booster current cannot be measured. Check SY current transformer')
            self.info('Booster current = {0:.3g}/{1:.3g}'.format(sycurrent, min_booster_current))
            if not (sycurrent > min_booster_current):
                raise OPEError('Booster current too low: {0:.3g}/{1:.3g} mA'.format(sycurrent, min_booster_current))
            self.gun.Off()
            stpargs['keep_running'] = False
            total_current = self.srct.value
            sb_current = self.srict.value
            if (current_ref <= 0 and sb_current_ref <= 0) or (
                    total_current > current_ref and sb_current > sb_current_ref):
                raise OPEError('SR current too high: {0:.3g}/{1:.3g} mA'.format(total_current, current_ref))
            move_gaps = ((current_ref - total_current) > self.idpower.MaxInjectedCurrentAuthorized)
            if move_gaps:
                self.error('Setting insertion devices to injection mode')
                #self.macros.IDCommand('Injection')  # Set gaps to injection if necessary
                self.GroupCommand(self.idservers,'Injection')
                self.GroupCommand(self.scrapers, 'Injection')
            self.kickelems.command_inout('On')  # Start kickers and septa
            time.sleep(3.3)  # Time for the injection septa to get their nominal value
            self.macros.Refill(current_ref, sb_current_ref, margin, sycurrent, timeout)
        except DevFailed as deverr:  # Tango device error
            self.deverror(deverr, 'Injection failed:')
            stp.start()
            self.try_(self.gun.Off, message='turning gun off')
        except OPEError as err:  # Booster current too low, SR current too high
            self.error('Injection skipped: ' + str(err))
            stp.start()
            self.try_(self.gun.Off, message='turning gun off')
        except Exception as err:  # Errors from refill (unknown mode,...)
            self.error('Injection failed: ' + str(err))
            stp.start()
            self.try_(self.gun.Off, message='turning gun off')
        else:
            if cleaning:
                stpargs['cleaning_scrapers'] = self.SRclean.Scrapers
                stp.start()
                self.macros.Clean()
            else:
                stp.start()
            counter = 'Success'
        finally:
            if move_gaps:
                #self.macros.IDCommand('Delivery')  # Set gaps to Delivery if necessary
                self.GroupCommand(self.idservers,'Delivery')
            self.set_counter()  # Reset the users's countdown
            self.output('Injection done in {0:.1f} seconds'.format(time.time() - self.start_time))
            if cleaning:
                with self.context('Info'):
                    #self.try_(self.srinj.scrapers_delivery, move_scrapers=move_scrapers,
                    #          message='setting scrapers for delivery')
                    self.GroupCommand(self.scrapers, 'Delivery')
            stp.join()
            self.output('')
            self.setEnv(counter, self.getEnv(counter, 0) + 1)

    def stop_booster(self, move_scrapers=True, keep_running=False, cleaning_scrapers=-1):
        self.macros.SRInjOff(move_scrapers, True, cleaning_scrapers)  # Open all scrapers except those used for cleaning
        if self.getEnv('EnableRips', True):
            self.try_(self.rips.StopRamping, message="stopping RIPS ramp")
        if not keep_running:
            delay = self.getEnv('CheckDelay', 0)
            if delay > 0:
                threading.Timer(delay, self.check).start()
            self.macros.SYPSOff()
            self.macros.SYRFOff()
        self.macros.SYExtOff(False)
        self.macros.LinacOff()
        self.macros.SYInjOff(False)
        if self.getEnv('EnableSYCleaning', False):
            self.SYclean.Off()

    def check(self):
        self.info('Checking SYRF and BPSS')
        if self.getEnv('EnableSyrf', True) and self.syrf.state() != DevState.STANDBY:
            self.error('Topup alarm: SYRF is not in Standby')
        if self.getEnv('EnableBpss', True) and (
                self.bpss.state() != DevState.ON or self.bpss.SettingsFileName != 'Economy'):
            self.error('Topup alarm: BPSS is not in Economy')
        if self.getEnv('EnableLinac', True) and self.linac.state() != DevState.STANDBY:
            self.error('Topup alarm: Linac is not in Standby')


class StartTopUp(TopupMacro):
    def run(self):
        TopupMacro.sy_clean = True
        rips = (Device('sy/ps-rips-switch/src').Source == 2)
        syps_ready = self.RIPS_ready if rips else self.BPSS_ready
        try:
            wait_for(partial(syps_ready, run=False),  # 81 s
                     partial(self.SYRF_ready, run=False),  # 53 s
                     partial(self.linac_ready, run=False),
                     timeout=450)
        except TimeoutError:
            self.error("Timeout preparing top-up")
        except DevFailed as deverr:
            self.deverror(deverr, 'Error preparing top-up:')
        except OPEError as opeerr:
            self.error('{0}'.format(opeerr))
        else:
            self.set_counter()
            self.cron[0].On()


class StopTopUp(TopupMacro):
    def run(self):
        self.cron[0].Off()


class ResetCounter(TopupMacro):
    param_def = [
        ["Period", Type.Integer, 0, "Injection periodicity"],
        ["CronId", Type.Integer, 0, "Countdown cron identifier"]
    ]

    def run(self, period, cronid):
        self.set_counter(period if period > 0 else None, cronid)


class ResetStatistics(TopupMacro):
    def run(self):
        self.setEnv('Success', 0)
        self.setEnv('Failure', 0)


OPEMacro.dbgmessage('Imported ' + __name__)  # Done on "ReloadMacroLib"
