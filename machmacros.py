"""Standard ASD macros for any macroserver
"""
from __future__ import print_function

import sys
import os
import PyTango
import sardana
from sardana.macroserver.macro import Type
from OPEMacro import OPEMacro
from pymach.tango import Device, Attribute


class TestEnv(OPEMacro):
    """Print the environment values"""

    param_def = [["verbose", Type.Boolean, False, "verbose output"]]

    def run(self, verbose):
        self.output("      Python : {0}".format(".".join([str(i) for i in sys.version_info[:3]])))
        self.output("       Tango : {0}".format(PyTango.constants.TgLibVers))
        self.output("     PyTango : {0}".format(PyTango.__version__))
        self.output("     Sardana : {0}".format(sardana.Release.version))
        self.output("   door name : {0}".format(self.getDoorName()))
        self.output("         pwd : {0}".format(os.getcwd()))

        for key, value in self.getAllEnv().iteritems():
            self.print('{0:>12s} : {1}'.format(key, value))

        if verbose:
            self.output('all: ' + str(self.getAllEnv()))
            self.output('Door: ' + str(self.getAllDoorEnv()))
            self.output('Local: ' + str(self.getEnv(None)))
            self.output('\nsys.path:\n' + '\n'.join(sys.path))
            self.output('\nPYTHONPATH:\n' + '\n'.join(os.environ['PYTHONPATH'].split(':')))
            self.output('\nPATH:\n' + '\n'.join(os.environ['PATH'].split(':')))
            self.output('\nLD_LIBRARY_PATH:\n' + '\n'.join(os.environ['LD_LIBRARY_PATH'].split(':')))


class GetEnv(OPEMacro):
    """Check environment variable"""

    param_def = [["key", Type.String, None, "Environment variable name"]]

    def run(self, key):
        value = self.getEnv(key)
        self.print('{0}: {1}, {2}'.format(key, value, type(value)))


class SetBooleanEnv(OPEMacro):
    """Set any environment flag"""

    param_def = [["Key", Type.String, None, "Environment variable name"],
                 ["Value", Type.Boolean, True, "flag"]]

    def run(self, key, value):
        self.setEnv(key, value)


class SetFloatEnv(OPEMacro):
    """Set any environment flag"""

    param_def = [["Key", Type.String, None, "Environment variable name"],
                 ["Value", Type.Float, 0.0, "Float value"]]

    def run(self, key, value):
        self.setEnv(key, value)


class SetStringEnv(OPEMacro):
    """Set any environment flag"""

    param_def = [["Key", Type.String, None, "Environment variable name"],
                 ["Value", Type.String, "", "string value"]]

    def run(self, key, value):
        self.setEnv(key, value)


class SetIntegerEnv(OPEMacro):
    """Set any environment flag"""

    param_def = [["Key", Type.String, None, "Environment variable name"],
                 ["Value", Type.Integer, 0, "Integer value"]]

    def run(self, key, value):
        self.setEnv(key, value)


class UnsetEnv(OPEMacro):
    """Check environment variable"""

    param_def = [["Key", Type.String, None, "Environment variable name"]]

    def run(self, key):
        self.unsetEnv(key)


class SetNotif(OPEMacro):
    """Set the notification device for Output:
    'talk'	: Talker
    'file'  : Append to file
    'growl'	: Growl on maclf.esrf.fr
    """

    param_def = [
        ['Queue', Type.String, 'Output', 'Queue name'],
        ['Notifiers', [['Notifier', Type.String, '', "Notification handler"], {'min': None, 'max': None}],
         None, 'List of notification handlers'],
    ]

    def run(self, queue, notifiers):
        self._set_notif(queue, *notifiers)


class GetNotif(OPEMacro):
    param_def = [
        ['Queue', Type.String, '', 'Queue name'],
    ]

    def run(self, queue):
        if len(queue) > 0:
            self.print(repr(self.notifier[queue]))
        else:
            for queue in self.notifier:
                self.print('{0:8}:'.format(queue), repr(self.notifier[queue]))


class SetOutNotif(OPEMacro):
    """Set the notification device for Output:
    'talk'	: Talker
    'file'  : Append to file
    'growl'	: Growl on maclf.esrf.fr

    Default: []
    """

    param_def = [
        ['Notifiers', [['Notifier', Type.String, '', "Notification handler"], {'min': None, 'max': None}],
         None, 'List of notification handlers'],
    ]

    def run(self, notifiers):
        self._set_notif('Output', *notifiers)


class SetErrNotif(OPEMacro):
    """Set the notification device for Error:
    'talk'	: Talker
    'file'  : Append to file
    'growl'	: Growl on maclf.esrf.fr

    Default: [file, talk]
    """

    param_def = [
        ['Notifiers', [['Notifier', Type.String, '', "Notification handler"], {'min': None, 'max': None}],
         None, 'List of notification handlers'],
    ]

    def run(self, notifiers):
        self._set_notif('Error', *notifiers)


class SetLevel(OPEMacro):
    param_def = [["Level", Type.Integer, '', "Debug level"]]

    def run(self, level):
        self._set_level(level)


class Critical(OPEMacro):
    """Test the Critical channel"""
    param_def = [["Message", Type.String, "Test critical message", "Critical string"]]

    def run(self, message):
        self.critical(message)


class Error(OPEMacro):
    """Test the Error channel"""
    param_def = [["Message", Type.String, "Test error message", "Error string"]]

    def run(self, message):
        self.error(message)


# noinspection PyShadowingBuiltins
class Warning(OPEMacro):
    """Test the Warning channel"""
    param_def = [["Message", Type.String, "Test warning message", "Warning string"]]

    def run(self, message):
        self.warning(message)


class Output(OPEMacro):
    """Test the Output channel"""
    param_def = [["Message", Type.String, "Test output message", "Output string"]]

    def run(self, message):
        self.output(message)


class Info(OPEMacro):
    """Test the Info channel"""
    param_def = [["Message", Type.String, "Test info message", "Info string"]]

    def run(self, message):
        self.info(message)


class Debug(OPEMacro):
    """Test the Debug channel"""
    param_def = [
        ["Message", Type.String, "Test error message", "Error string"],
        ["Level", Type.Integer, 255, "Debug level"]
    ]

    def run(self, message, level):
        self.debug(message, level=level)


class DevCommand(OPEMacro):
    clname = 'topup'

    param_def = [
        ['dev_name', Type.String, 'sr/d-ct/1', 'Device name'],
        ['cmd_name', Type.String, 'State', 'Command name'],
        ['Arg1', Type.String, '', 'Command argument'],
        ['Arg2', Type.String, '', 'Command argument'],
        ['Arg3', Type.String, '', 'Command argument'],
        ['Arg4', Type.String, '', 'Command argument'],
        ['Arg5', Type.String, '', 'Command argument'],
        ['Arg6', Type.String, '', 'Command argument']
    ]

    def run(self, dev_name, cmd_name, *args):
        ag = [a for a in args if len(a) > 0]
        [self.print(a, type(a)) for a in ag]
        result = self.try_(Device(dev_name).command_inout, cmd_name)
        self.print(result)


class DevAttribute(OPEMacro):
    clname = 'topup'

    param_def = [
        ['attr_name', Type.String, 'sr/d-ct/1/current', 'Attribute name'],
    ]

    def run(self, attr_name):
        result = self.try_(lambda: Attribute(attr_name).setread())
        self.print(result)


OPEMacro.dbgmessage('Imported ' + __name__)  # Done on "ReloadMacroLib"
