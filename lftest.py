"""This module contains macros that demonstrate the usage of macro parameters"""

from __future__ import print_function

import time
import os
from pymach.timing import wait_for
from sardana.macroserver.macro import Macro
from sardana.macroserver.macro import Type

from OPEMacro import OPEMacro

_FILLING_PROFILE_NAME = os.path.join(os.environ['APPHOME'], 'topup', 'topup.mat')


class LFMacro(OPEMacro):
    """Base topup macro"""

    @staticmethod
    def getids(n):
        OPEMacro.dbgmessage('Class preparation')
        return n

    def __init__(self, *args, **kwargs):
        super(LFMacro, self).__init__(*args, **kwargs)
        self.start_time = time.time()


class MultiPar(Macro):
    """Macro with a motor parameter followed by a list of numbers. The list as
    explicitly stated an optional last element which is a dictionary that defines the
    min and max values for repetitions"""

    param_def = (
        ('numb_list', (('pos', Type.Integer, 0, 'value'), {'min': None, 'max': None}), None, 'List of values'),
    )

    def run(self, numb_list):
        self.output('{0} argument[s]'.format(len(numb_list)))
        for num in numb_list:
            self.output('{0}:{1!r}'.format(type(num), num))


class MultiCall(Macro):
    """

    """

    param_def = [['flag', Type.Integer, 2, 'test']]

    def run(self, nparams):
        self.output('{0} parameters[s]'.format(nparams))
        pars = range(nparams)
        self.macros.MultiPar(*pars)


class Mtest(OPEMacro):

    param_def = [['flag', Type.Boolean, True, 'test']]

    class Truc(object):
        def __init__(self):
            OPEMacro.dbgmessage('Initializing instance of ' + self.__class__.__name__)

        def __del__(self):
            OPEMacro.dbgmessage('Deleting instance of ' + self.__class__.__name__)

    def __init__(self, *args, **kwargs):
        super(Mtest, self).__init__(*args, **kwargs)
        OPEMacro.dbgmessage('Initializing instance of ' + self.__class__.__name__)
        self.output('Initializing instance of ' + self.__class__.__name__)
        self.data = Mtest.Truc()

    def __del__(self):
        OPEMacro.dbgmessage('Deleting instance of ' + self.__class__.__name__)
        self.output('Deleting instance of ' + self.__class__.__name__)

    def prepare(self, flag):
        self.output('Preparing ' + self.__class__.__name__)

    def run(self, flag):
        self.output('Running ' + self.__class__.__name__)
        self.print(self.fname('Output'))


class LFTestAbort(OPEMacro):
    def run(self):
        self.output('{0}'.format(self._notif['Output']))
        wait_for(lambda: False, timeout=2)


class LFRun2(LFMacro):
    param_def = [
        ["Current", Type.Float, 200, "Desired SR current"],
        ["SbCurrent", Type.Float, 0, "Desired single bunch current"],
        ["Timeout", Type.Float, 10, "Injection Max. duration"], ]

    def run(self, current, sb_current, timeout):
        self.notifier.debug('Before testproc', level=1)
        # self.subcmd(['testproc.py', str(current)])
        self.subcmd(['tsub', 'On', 'Off', 'Status'])
        # self.subcmd(['tsubsub', 'On', 'Off', 'Status'])
        self.notifier.debug('After  testproc', level=1)


class LFQueue(LFMacro):
    def run(self):
        self.print('This is a multi-line string:\nline 1\nline 2')
        [getattr(self, f)("test " + f) for f in ('debug', 'output', 'warning', 'error')]
        [getattr(self.notifier, f)("notify on " + f) for f in
         ('debug', 'info', 'output', 'warning', 'error', 'critical')]


class Sleep(LFMacro):
    param_def = [["Sleep", Type.Integer, 20, "Sleep time [s]"]]

    def run(self, sleeptime):
        self.output('Go')
        t0 = time.time()
        self.sleep(sleeptime)
        self.output('Done in {0}'.format(time.time()-t0))


OPEMacro.dbgmessage('Imported ' + __name__)  # Done on "ReloadMacroLib"
